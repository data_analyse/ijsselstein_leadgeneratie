CREATE TABLE p_bedrijf_ijsselstein.kvk_obv_geo AS (
SELECT a.vestigingsnummer_kvk
, b.rin_nummer
FROM d_kvk.kvk_geo_bag a
RIGHT JOIN p_bedrijf_ijsselstein.bedrijventerreinen b
ON ST_Intersects(a.geom_wgs,b.geom_wgs)
)
;

CREATE INDEX IF NOT EXISTS idx_ijsselstein_geocodering_vestigingsnummer ON p_bedrijf_ijsselstein.geocodering(vestigingsnummer_kvk);

-- Check ontbrekende bedrijven op basis van postcode.
-- Alleen postcodes met minimaal 20% op bedrijventerrein meenemen
CREATE TABLE p_bedrijf_ijsselstein.kvk_obv_postcode AS (
SELECT 
a.vestigingsnummer_kvk
, b.rin_nummer
, b.overlap_percentage
, Max(b.overlap_percentage) as grootste_overlap
FROM d_kvk.kvk_export a
RIGHT JOIN p_bedrijf_ijsselstein.postcode_bedrijventerrein_koppeling b
ON a.postcode_kvk = b.pc6
WHERE b.overlap_percentage >= 0.2
GROUP BY
a.vestigingsnummer_kvk
, b.rin_nummer
, b.overlap_percentage
)
;

-- KvK op bedrijventerreinen
CREATE TABLE p_bedrijf_ijsselstein.kvk_selectie AS (
SELECT
vestigingsnummer_kvk
, rin_nummer
, 1 as geo_koppeling
, 0 as postcode_koppeling
FROM p_bedrijf_ijsselstein.kvk_obv_geo
UNION
SELECT
vestigingsnummer_kvk
, rin_nummer
, 0 as geo_koppeling
, 1 as postcode_koppeling
FROM p_bedrijf_ijsselstein.kvk_obv_postcode
WHERE overlap_percentage = grootste_overlap
)
;

CREATE TABLE p_bedrijf_ijsselstein.kvk_bedrijventerrein_koppeling AS (
SELECT
vestigingsnummer_kvk
, rin_nummer
, SUM(geo_koppeling) as geo_koppeling
, SUM(postcode_koppeling) as postcode_koppeling
FROM p_bedrijf_ijsselstein.kvk_selectie
GROUP BY
vestigingsnummer_kvk
, rin_nummer
)
;

DROP TABLE p_bedrijf_ijsselstein.geocodering;
CREATE TABLE p_bedrijf_ijsselstein.geocodering AS (
SELECT 
a.id_kvk
, a.naam_kvk
, a.adres_kvk
, a.huisnummer_kvk
, a.huisletter_kvk
, a.toevoeging_kvk
, a.postcode_kvk
, a.plaats_kvk
, a.kvknummer_kvk
, a.vestigingsnummer_kvk
, a.vbo_id
, a.pand_id
, a.hoofdadres_straatnaam
, a.hoofdadres_identificatie
, a.hoofdadres_huisnummer
, a .hoofdadres_huisletter
, a.hoofdadres_huisnummertoevoeging
, a.hoofdadres_postcode
, a.hoofdadres_volledig_adres
, a.nevenadres_identificatie
, a.woonplaats
, a.gemeentenaam
, a.gm_code
, a.provincienaam
, a.geom_wgs
, a.geom_rd
, a.koppeling_prio
, a.aantal_koppelingen
FROM d_kvk.kvk_geo_bag a
RIGHT JOIN p_bedrijf_ijsselstein.kvk_bedrijventerrein_koppeling b
ON a.vestigingsnummer_kvk = b.vestigingsnummer_kvk
GROUP BY
a.id_kvk
, a.naam_kvk
, a.adres_kvk
, a.huisnummer_kvk
, a.huisletter_kvk
, a.toevoeging_kvk
, a.postcode_kvk
, a.plaats_kvk
, a.kvknummer_kvk
, a.vestigingsnummer_kvk
, a.vbo_id
, a.pand_id
, a.hoofdadres_straatnaam
, a.hoofdadres_identificatie
, a.hoofdadres_huisnummer
, a .hoofdadres_huisletter
, a.hoofdadres_huisnummertoevoeging
, a.hoofdadres_postcode
, a.hoofdadres_volledig_adres
, a.nevenadres_identificatie
, a.woonplaats
, a.gemeentenaam
, a.gm_code
, a.provincienaam
, a.geom_wgs
, a.geom_rd
, a.koppeling_prio
, a.aantal_koppelingen
)
;


CREATE INDEX IF NOT EXISTS idx_ijsselstein_geocodering_pandid ON p_bedrijf_ijsselstein.geocodering(pand_id);
CREATE INDEX IF NOT EXISTS idx_ijsselstein_geocodering_vboid ON p_bedrijf_ijsselstein.geocodering(vbo_id);
CREATE INDEX IF NOT EXISTS idx_ijsselstein_geocodering_idkvk ON p_bedrijf_ijsselstein.geocodering(id_kvk);
CREATE INDEX IF NOT EXISTS sidx_ijsselstein_geocodering_geomwgs ON p_bedrijf_ijsselstein.geocodering USING GIST(geom_wgs);

